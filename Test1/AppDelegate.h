//
//  AppDelegate.h
//  Test1
//
//  Created by Razvan on 12/01/15.
//  Copyright (c) 2015 adonis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

