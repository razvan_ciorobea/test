//
//  main.m
//  Test1
//
//  Created by Razvan on 12/01/15.
//  Copyright (c) 2015 adonis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
